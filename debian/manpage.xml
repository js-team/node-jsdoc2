<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [

<!--

`xsltproc -''-nonet \
          -''-param man.charmap.use.subset "0" \
          -''-param make.year.ranges "1" \
          -''-param make.single.year.ranges "1" \
          /usr/share/xml/docbook/stylesheet/docbook-xsl/manpages/docbook.xsl \
          manpage.xml`

A manual page <package>.<section> will be generated. You may view the
manual page with: nroff -man <package>.<section> | less'. A typical entry
in a Makefile or Makefile.am is:

DB2MAN = /usr/share/sgml/docbook/stylesheet/xsl/docbook-xsl/manpages/docbook.xsl
XP     = xsltproc -''-nonet -''-param man.charmap.use.subset "0"

manpage.1: manpage.xml
        $(XP) $(DB2MAN) $<

The xsltproc binary is found in the xsltproc package. The XSL files are in
docbook-xsl. A description of the parameters you can use can be found in the
docbook-xsl-doc-* packages. Please remember that if you create the nroff
version in one of the debian/rules file targets (such as build), you will need
to include xsltproc and docbook-xsl in your Build-Depends control field.
Alternatively use the xmlto command/package. That will also automatically
pull in xsltproc and docbook-xsl.

Notes for using docbook2x: docbook2x-man does not automatically create the
AUTHOR(S) and COPYRIGHT sections. In this case, please add them manually as
<refsect1> ... </refsect1>.

To disable the automatic creation of the AUTHOR(S) and COPYRIGHT sections
read /usr/share/doc/docbook-xsl/doc/manpages/authors.html. This file can be
found in the docbook-xsl-doc-html package.

Validation can be done using: `xmllint -''-noout -''-valid manpage.xml`

General documentation about man-pages and man-page-formatting:
man(1), man(7), http://www.tldp.org/HOWTO/Man-Page/

-->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "Georges">
  <!ENTITY dhsurname   "Khaznadar">
  <!-- dhusername could also be set to "&dhfirstname; &dhsurname;". -->
  <!ENTITY dhusername  "georgesk">
  <!ENTITY dhemail     "georgesk@debian.org">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1) and
       http://www.tldp.org/HOWTO/Man-Page/q2.html. -->
  <!ENTITY dhsection   "1">
  <!-- TITLE should be something like "User commands" or similar (see
       http://www.tldp.org/HOWTO/Man-Page/q2.html). -->
  <!ENTITY dhtitle     "jsdoc2 User Manual">
  <!ENTITY dhucpackage "JSDOC2">
  <!ENTITY dhpackage   "jsdoc2">
]>

<refentry>
  <refentryinfo>
    <title>&dhtitle;</title>
    <productname>&dhpackage;</productname>
    <authorgroup>
      <author>
       <firstname>&dhfirstname;</firstname>
        <surname>&dhsurname;</surname>
        <contrib>Wrote this manpage for the Debian system.</contrib>
        <address>
          <email>&dhemail;</email>
        </address>
      </author>
    </authorgroup>
    <copyright>
      <year>2024</year>
      <holder>&dhusername;</holder>
    </copyright>
    <legalnotice>
      <para>This manual page was written for the Debian system
        (and may be used by others).</para>
      <para>Permission is granted to copy, distribute and/or modify this
        document under the terms of the GNU General Public License,
        Version 2 or (at your option) any later version published by
        the Free Software Foundation.</para>
      <para>On Debian systems, the complete text of the GNU General Public
        License can be found in
        <filename>/usr/share/common-licenses/GPL</filename>.</para>
    </legalnotice>
  </refentryinfo>
  <refmeta>
    <refentrytitle>&dhucpackage;</refentrytitle>
    <manvolnum>&dhsection;</manvolnum>
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>
    <refpurpose>Automatic documentation generation tool for JavaScript</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
      <arg choice="opt"><option>OPTIONS</option></arg>
      <arg><replaceable>&lt;SRC_DIR&gt;</replaceable></arg>
      <arg><replaceable>&lt;SRC_FILE&gt;</replaceable></arg>
      <arg>...</arg>
    </cmdsynopsis>
    <cmdsynopsis>
      <command>&dhpackage;</command>
      <arg choice="plain"><option>--help</option></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1 id="usage">
    <title>USAGE</title>
    <para>
      A valid command line to run <command>jsdoc2</command> might look like this:
    </para>
    <para>
      <command>jsdoc2 -a -t=templates/jsdoc mycode.js</command>
    </para>
  </refsect1>
  <refsect1 id="description">
    <title>DESCRIPTION</title>
    <para>This manual page documents briefly the
      <command>&dhpackage;</command>
      command.</para>
      <para>
	This manual page was written for the Debian distribution
	because the original program does not have a manual page.
      </para>
      <para>
	<command>&dhpackage;</command>
	is an automatic documentation generation tool for JavaScript.
	It is written in JavaScript and is run from a command line
	(or terminal) and uses NodeJS.
      </para>
      <para>
	Using this tool you can automatically turn JavaDoc-like comments
	in your JavaScript source code into published output files,
	such as HTML or XML.
      </para>
  </refsect1>
  <refsect1 id="options">
    <title>OPTIONS</title>
    <variablelist>
      <!-- Use the variablelist.term.separator and the
           variablelist.term.break.after parameters to
           control the term elements. -->
      <varlistentry>
        <term><option>-a</option></term>
        <term><option>--allfunctions</option></term>
        <listitem>
          <para>
	    Include all functions, even undocumented ones.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-c</option></term>
        <term><option>--conf</option></term>
        <listitem>
          <para>
	    Load a configuration file.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-d=<replaceable>PATH</replaceable></option></term>
        <term><option>--directory=<replaceable>PATH</replaceable></option></term>
        <listitem>
          <para>
	    Output to this directory (defaults to "out").
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-D=<replaceable>"myVar:My value"</replaceable></option></term>
        <term><option>--define=<replaceable>"myVar:My value"</replaceable></option></term>
        <listitem>
          <para>
	    Multiple. Define a variable, available in JsDoc as JSDOC.opt.D.myVar.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-e=<replaceable>ENCODING</replaceable></option></term>
        <term><option>--encoding=<replaceable>ENCODING</replaceable></option></term>
        <listitem>
          <para>
	    Use this encoding to read and write files.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-E=<replaceable>REGEX</replaceable></option></term>
        <term><option>--exclude=<replaceable>REGEX</replaceable></option></term>
        <listitem>
          <para>
	    Multiple. Exclude files based on the supplied regex.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-h</option></term>
        <term><option>--help</option></term>
        <listitem>
          <para>
	    Show summary of options.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-m</option></term>
        <term><option>--multiples</option></term>
        <listitem>
          <para>
	    Don't warn about symbols being documented more than once.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-n</option></term>
        <term><option>--nocode</option></term>
        <listitem>
          <para>
	    Ignore all code, only document comments with @name tags.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-o=<replaceable>PATH</replaceable></option></term>
        <term><option>--out=<replaceable>PATH</replaceable></option></term>
        <listitem>
          <para>
	    Print log messages to a file (defaults to stdout).
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-p</option></term>
        <term><option>--private</option></term>
        <listitem>
          <para>
	    Include symbols tagged as private, underscored and inner symbols.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-q</option></term>
        <term><option>--quiet</option></term>
        <listitem>
          <para>
	    Do not output any messages, not even warnings.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-r=<replaceable>DEPTH</replaceable></option></term>
        <term><option>--recurse=<replaceable>DEPTH</replaceable></option></term>
        <listitem>
          <para>
	    Descend into src directories.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-s</option></term>
        <term><option>--suppress</option></term>
        <listitem>
          <para>
	    Suppress source code output.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-S</option></term>
        <term><option>--securemodules</option></term>
        <listitem>
          <para>
	    Use Secure Modules mode to parse source code.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-t=<replaceable>PATH</replaceable></option></term>
        <term><option>--template=<replaceable>PATH</replaceable></option></term>
        <listitem>
          <para>
	    Required. Use this template to format the output.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-T</option></term>
        <term><option>--test</option></term>
        <listitem>
          <para>
	    Run all unit tests and exit.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-u</option></term>
        <term><option>--unique</option></term>
        <listitem>
          <para>
	    Force file names to be unique, but not based on symbol names.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-v</option></term>
        <term><option>--verbose</option></term>
        <listitem>
          <para>
	    Provide verbose feedback about what is happening.
	  </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-x=<replaceable>&lt;EXT&gt;[,EXT]...</replaceable></option></term>
        <term><option>--ext=<replaceable>&lt;EXT&gt;[,EXT]...</replaceable></option></term>
        <listitem>
          <para>
	    Scan source files with the given extension/s (defaults to js).
	  </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
</refentry>

